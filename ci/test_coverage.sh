#!/usr/bin/env sh

# Skript zur Ermittlung welche Testfälle definiert sind
# und welche aufgerufen werden.

set -e

# Liste aller definierten Testfälle
def_testcases=$(yq -N '.metadata.name ' tests/*.yaml tests/policies/*/*-resource.yaml | sort -u)
count_def_testcases=$(printf '\n%s' $def_testcases | wc -l)

printf "\033[0;32mAnzahl definierter Testfälle:\033[0m "
printf "$count_def_testcases\n\n"

# Liste aller aufgerufenen Testfälle
used_testcases=$(yq -N '.results[].resources | .[]' kyverno-tests/*/kyverno-test.yaml tests/policies/*/kyverno-test.yaml | sort -u)
count_used_testcases=$(printf '\n%s' $used_testcases | wc -l)

printf "\033[0;32mAnzahl an aufgerufenen Testfällen:\033[0m "
printf "$count_used_testcases\n\n"

printf '\n%s' $def_testcases > def_testcases.txt
printf '\n%s' $used_testcases > used_testcases.txt

# ersten 4 Zeilen und das "-" von der diff Ausgabe entfernen
printf "\033[0;31m❌ Nicht aufgerufene Testfälle:\033[0m\n"
diff -U0 def_testcases.txt used_testcases.txt | tail -n +4 | sed -r 's/^-{1}//'

printf "\n\n\033[0;32mCoverage: $(printf "scale = 2; $count_used_testcases / $count_def_testcases * 100\n" | bc)%%\033[0m\n"
