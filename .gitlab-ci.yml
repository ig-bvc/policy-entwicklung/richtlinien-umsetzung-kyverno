variables:
  KYVERNO_VERSION: v1.11.4
  KUSTOMIZE_VERSION: v4.5.7

stages:
  - validate
  - test
  - build

# https://docs.gitlab.com/ee/ci/yaml/workflow.html#switch-between-branch-pipelines-and-merge-request-pipelines
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH

runYamllint:
  stage: validate
  image:
    name: alpine:3.19
  rules:
    - changes:
        - "**/*.y*ml"
        - .gitlab-ci.yml
      allow_failure: true
  script:
    - apk add --no-cache yamllint
    - yamllint -c $CI_PROJECT_DIR/.yamllint .

runMarkdownLinkCheck:
  stage: validate
  image:
    name: ghcr.io/tcort/markdown-link-check:3.11.2
    entrypoint: ["/bin/sh", "-c"]
  rules:
    - changes:
        - "**/*.md"
      allow_failure: true
  script:
    - find . -name \*.md -print0 | xargs -0 -n1 markdown-link-check

test-coverage:
  image: mikefarah/yq:4
  stage: test
  script:
    - ci/test_coverage.sh
  coverage: '/Coverage:\s*\d+\.\d+/'
  allow_failure: true
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

test-policies:
  image: busybox:latest
  stage: test
  script:
    - echo ">>> Install Kyverno"
    - wget -P /tmp/ https://github.com/kyverno/kyverno/releases/download/${KYVERNO_VERSION}/kyverno-cli_${KYVERNO_VERSION}_linux_x86_64.tar.gz
    - tar -xvf /tmp/kyverno-cli_${KYVERNO_VERSION}_linux_x86_64.tar.gz
    - echo ">>> Kyverno version"
    - ./kyverno version
    - echo ">>> Run Tests"
    - ./kyverno test ${CI_PROJECT_DIR}/kyverno-tests/ ${CI_PROJECT_DIR}/tests/policies/

render-release-policies:
  image: busybox:latest
  stage: build
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo ">>> Install Kustomize"
    - wget -P /tmp/ https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2F${KUSTOMIZE_VERSION}/kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz
    - tar -xvf /tmp/kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz
    - echo ">>> Kustomize version"
    - ./kustomize version
    - echo ">>> Build flavored releases"
    - mkdir -p ${CI_PROJECT_DIR}/release
    - cd policy-flavors
    - for flavor_dir in */ ; do
        flavor=${flavor_dir::-1};
        echo ">>> Building ${flavor}";
        ../kustomize build ${flavor_dir} > ${CI_PROJECT_DIR}/release/igbvc-policies-${flavor}.yaml;
      done
    - 'correct_tag="policies.opencode.de/release-version: ${CI_COMMIT_TAG:1}"'
    - if grep -Fq "$correct_tag" ${CI_PROJECT_DIR}/release/igbvc-policies-cluster-enforce.yaml;
      then
        echo "✅ Policies annotated with correct release version.";
      else
        echo "❌ policies.opencode.de/release-version does not match release tag (should be ${CI_COMMIT_TAG:1}).";
        exit 1;
      fi
  artifacts:
    paths:
      - ${CI_PROJECT_DIR}/release
