apiVersion: kyverno.io/v1
kind: ClusterPolicy
metadata:
  name: disallow-latest-tag
  annotations:
    policies.opencode.de/ID: "007"
    policies.opencode.de/bsi-requirement: SYS.1.6.A6
    policies.kyverno.io/title: Disallow Latest Tag
    policies.kyverno.io/category: Best Practices
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod
    policies.kyverno.io/description: >-
      The ':latest' tag is mutable and can lead to unexpected errors if the
      image changes. A best practice is to use an immutable tag that maps to
      a specific version of an application Pod. This policy validates that the image
      specifies a tag and that it is not called `latest`.
  labels:
    policies.opencode.de/bsi-protection-requirement: basic
    policies.opencode.de/category: must

spec:
  validationFailureAction: Audit
  background: true
  rules:
    - name: require-image-tag
      match:
        any:
          - resources:
              kinds:
                - Pod
      validate:
        message: "An image tag is required."
        pattern:
          spec:
            =(ephemeralContainers):
              - =(image): "*:*"
            =(initContainers):
              - =(image): "*:*"
            containers:
              - image: "*:*"

    - name: validate-image-tag
      match:
        any:
          - resources:
              kinds:
                - Pod
      validate:
        message: "Using a mutable image tag e.g. 'latest' is not allowed."
        pattern:
          spec:
            =(ephemeralContainers):
              - =(image): "!*:latest"
            =(initContainers):
              - =(image): "!*:latest"
            containers:
              - image: "!*:latest"
